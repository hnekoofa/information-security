echo "Encoding Symetric..."
openssl enc -e -a -salt -bf -in swann.txt -out swannopensslsymetric.txt
echo "Decoding Symetric..."
openssl enc -d -a -salt -bf -in swannopensslsymetric.txt -out swannopensslsymetricdecrypted.txt
