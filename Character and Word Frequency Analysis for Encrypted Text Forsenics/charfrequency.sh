#/bin/bash
clear
echo "Running Plain now."
for line in `cat ascii.txt`; do 
grep "$line" swann.txt | wc -l >> plaintextcharfrequency.txt;
done;
echo "Running symetric now"
for line in `cat ascii.txt`; do
grep "$line" swannsymetric.txt | wc -l >>symetriccharfrequency.txt;
done;
echo "Running pgp now"
for line in `cat ascii.txt`; do
grep "$line" swannpgp.txt | wc -l >> pgpcharfrequency.txt;
done;
echo "Done"
