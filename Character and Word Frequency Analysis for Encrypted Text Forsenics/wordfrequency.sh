#/bin/bash
clear
echo "Finding the most frequent words"
cat swann.txt 	         |
tr -cs A-Za-z\' '\n' 	 |
tr A-Z a-z		 |
sort 			 |
uniq -c   		 |
sort -k1,1nr -k2 	 |
sed '1,64!d' > wordfrequency.txt
echo `cat wordfrequency.txt`

echo "Finding the most frequent 2-letter words"
cat swann.txt 	        |
tr -cs A-Za-z\' '\n' 	|
tr A-Z a-z		|
sed 's/ //'		|
awk 'length==2' 	|
sort 			|
uniq -c 		|
sort -k1,1nr -k2 	|
sed '1,32!d'  > twoletterfrequency.txt
echo `cat twoletterfrequency.txt`

echo "Finding the most frequent 3-letter words"
cat swann.txt 	        |
tr -cs A-Za-z\' '\n' 	|
tr A-Z a-z		|
sed 's/ //'		|
awk 'length==3' 	|
sort 			|
uniq -c 		|
sort -k1,1nr -k2 	|
sed '1,32!d'  > threeletterfrequency.txt
echo `cat threeletterfrequency.txt`

echo "Finding the most frequent 4-letter words"
cat swann.txt 	        |
tr -cs A-Za-z\' '\n' 	|
tr A-Z a-z		|
sed 's/ //'		|
awk 'length==4' 	|
sort 			|
uniq -c 		|
sort -k1,1nr -k2 	|
sed '1,32!d'  > fourletterfrequency.txt
echo `cat fourletterfrequency.txt`

echo "Finding the most frequent 5-letter words"
cat swann.txt 	        |
tr -cs A-Za-z\' '\n' 	|
tr A-Z a-z		|
sed 's/ //'		|
awk 'length==5' 	|
sort 			|
uniq -c 		|
sort -k1,1nr -k2 	|
sed '1,32!d'  > fiveletterfrequency.txt
echo `cat fiveletterfrequency.txt`

echo "Finding most common bigrams"
cat swann.txt 		|
tr -cs A-Za-z\' '\n'	|
tr A-Z a-z		|
sed 's/ //'		|
sort 			|
uniq -c	 		>allwords.txt
rm -f bigrams.txt
bigramtime=`wc -l allwords.txt | sed 's/[^0-9]* //'|sed 's/ .*//'`
echo "Expect O("$bigramtime""*7") time..."
cat allwords.txt | while read line
do
	word=`echo "$line" | sed 's/^[0-9]* //'`
	freq=`echo "$line" | sed 's/ [^0-9]*//'`
	
	for ((i=0;i<${#word}-1;i++)) 
	do	

		echo $freq "     " ${word:(i):2} >> bigrams.txt 
	done
done
sort bigrams.txt 	|
sort -k1,1nr -k2	|
sed '1,32!d' 		>bigramfrequency.txt

echo "Finding most common trigrams"
rm -f trigrams.txt
echo "Expect O("$bigramtime""*7") time..."
cat allwords.txt | while read line
do
        word=`echo "$line" | sed 's/^[0-9]* //'`
        freq=`echo "$line" | sed 's/ [^0-9]*//'`
        for ((i=0;i<${#word}-1;i++))
        do
                trigram=${word:(i):3}
                length=${#trigram}
                if [ "$length" == "3" ];

                then
                echo $freq "    " $trigram >> trigrams.txt; 
                fi
        done
done
sort trigrams.txt       |
sort -k1,1nr -k2        |
sed '1,32!d'            >trigramfrequency.txt
