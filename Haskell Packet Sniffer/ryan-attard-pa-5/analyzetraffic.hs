import Network.Pcap
import System.Environment
import System.IO
import Data.Char
import Text.Printf
import System.Environment
import Control.Monad
import Text.Printf
import System.Exit
import Data.Maybe
import System.IO
import Data.List
import Foreign



data PacketFmt = PacketFmt {
                   ascii       :: String,
                   hexadecimal :: [Word8]
                 }
 deriving (Eq,Show)

initPktFmt a h =
  PacketFmt {
    ascii       = a,
    hexadecimal = h
}

main = do
    args <- getArgs
    spy <- openLive "en1" 128 True (1)
    s<-Network.Pcap.statistics spy
    loop spy (-1) (printPacketInfo (head args))
    putStrLn ("\nPackets recieved: " ++ (show $ statReceived s))



printPacketInfo ::String-> PktHdr -> Ptr Word8 -> IO ()
printPacketInfo argument ph bytep = do {
                    a  <- peekArray (fromIntegral (hdrCaptureLength ph)) bytep;
                    s  <- return $ map (\x -> if (x <= 126) then x else 46) a;
                    s' <- return $ map (\x -> chr (read $ show x)) s;
                    formatPacketMacandIP argument (initPktFmt s' a);
                    hFlush stdout
                                        }

octets :: Word32 -> [Word8]
octets w =
    [fromIntegral (w `shiftR` 24)
    , fromIntegral (w `shiftR` 16)
    , fromIntegral (w `shiftR` 8)
    , fromIntegral w
    ]

formatPacketMacandIP ::String-> PacketFmt -> IO ()
formatPacketMacandIP argument packet = do
 --Splitting X' removes header info
  let verbose=False
      ipstart=14


      transportstart= (ipstart+(4*(ihl)))
      (asciiheader,asciipayload') = splitAt (transportstart+12) a
      (hexheader,hexpayload') = splitAt 12 h
      (hexmacdest,hexmacsrc') = splitAt 6 hexheader
      (extraipinfo, ipvers') = splitAt ipstart h
      (protocol, protocolextra') = splitAt  1 (snd (splitAt (ipstart+9) h))
      --bits 1 and 2 are used for version 4 and 6
      version=(read $ show (((((((head (ipvers'))`clearBit` 0)`clearBit` 3) `clearBit` 4) `clearBit` 5)`clearBit` 6) `clearBit` 7) :: Int)
      iphlen=(if version==6 then 12 else 4)
      ipaddroffset=(if version==6 then 8 else 12)
      (ipheader, ipv4data') = splitAt  (2*iphlen) (snd (splitAt (ipstart+ipaddroffset) h))
      (ipsrc,ipdest') = splitAt iphlen ipheader
      --bits 4-7 are used for header length
      ihl= (if version==6 then 9 else (read $ show (((((head ipvers') `clearBit` 4)`clearBit` 5) `clearBit` 6)`clearBit` 7)  :: Int))
      (tcpheader, tcppayload')= (splitAt (12) (snd (splitAt (transportstart  ) h)))
      (tcpsrc,tcpdest)=splitAt 6 tcpheader
  unless (isInfixOf argument asciipayload' ) $ do
    case verbose of
            False-> do {
            case version of
                6-> do
                    mapM_ (printf "%02.2x:") (init (fmtHex ipsrc));
                    printf "%02.2x" (last (fmtHex ipsrc));
                    printf ":%d " (((read $ show (head tcpsrc) :: Int)`shiftL` 8)+((read $ show ((head (tail tcpsrc))):: Int)));
         --print Desintation Info
                    mapM_ (printf "%02.2x") (init (fmtHex ipdest'));
                    printf "%02.2x" (last (fmtHex ipdest'));
                    printf ":%d\n" (((read $ show (head(tail (tail tcpsrc))):: Int)`shiftL` 8)+((read $ show (head(tail(tail (tail tcpsrc)))):: Int)));
                otherwise-> do
                    mapM_ (printf "%d.") (init (fmtHex ipsrc));
                    printf "%d" (last (fmtHex ipsrc));
                    printf ":%d " (((read $ show (head tcpsrc) :: Int)`shiftL` 8)+((read $ show ((head (tail tcpsrc))):: Int)));
         --print Desintation Info
                    mapM_ (printf "%d.") (init (fmtHex ipdest'));
                    printf "%d" (last (fmtHex ipdest'));
                    printf ":%d\n" (((read $ show (head(tail (tail tcpsrc))):: Int)`shiftL` 8)+((read $ show (head(tail(tail (tail tcpsrc)))):: Int)));}
            True-> do {
        --Packet Version, IP Header Length,Protocol
                printf "IPVersion %d " ((read $ show version) :: Int);
                printf "IPHeaderL %d " ((read $ show ihl) :: Int);
                printf "Protocol: %s \n" (mapprotocol (read $ show (head protocol):: Int));
              case version of
                 6-> do
                    mapM_ (printf "%02.2x:") (init (fmtHex ipsrc));
                    printf "%02.2x" (last (fmtHex ipsrc));
                    printf ":%d " (((read $ show (head tcpsrc) :: Int)`shiftL` 8)+((read $ show ((head (tail tcpsrc))):: Int)));
         --print Desintation Info
                    mapM_ (printf "%02.2x") (init (fmtHex ipdest'));
                    printf "%02.2x" (last (fmtHex ipdest'));
                    printf ":%d\n" (((read $ show (head(tail (tail tcpsrc))):: Int)`shiftL` 8)+((read $ show (head(tail(tail (tail tcpsrc)))):: Int)))
                 otherwise-> do
                    mapM_ (printf "%d.") (init (fmtHex ipsrc));
                    printf "%d" (last (fmtHex ipsrc));
                    printf ":%d " (((read $ show (head tcpsrc) :: Int)`shiftL` 8)+((read $ show ((head (tail tcpsrc))):: Int)));
         --print Desintation Info
                    mapM_ (printf "%d.") (init (fmtHex ipdest'));
                    printf "%d" (last (fmtHex ipdest'));
                    printf ":%d\n" (((read $ show (head(tail (tail tcpsrc))):: Int)`shiftL` 8)+((read $ show (head(tail(tail (tail tcpsrc)))):: Int)));

            }
            --    printf "\nPayload: %s\n" asciipayload'
 where
  a = ascii packet
  h = hexadecimal packet
  fmtHex :: [Word8] -> [Int]
  fmtHex bytes = [ (read $ show y) | y <- bytes] :: [Int]
  fmtIrregular :: Int -> IO ()
  fmtIrregular z = do
    printf "\t"
    if (z-8) > 0 then
      fmtIrregular (z - 8)
     else
      return ()



mapprotocol p = case p of
                1->"ICMP"
                2->"IGMP"
                6->	"TCP"
                17->"UDP"
                41->"ENCAP"
                89->"OSPF"
                132->"SCTP"
                otherwise-> "?"
