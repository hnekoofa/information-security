import Data.Int
import HPrimeGen
import System.IO.Unsafe
import System.Random
import System.Environment
import Text.Printf
import MyGcd
import System.IO

main = do
    args <- getArgs
    argtoint(args)
argtoint a = do
        keygen (read (head a) :: Integer) (read (head (tail a)) :: Integer)
keygen :: Integer->Integer->IO ()
keygen a1 a2 = let n=a1*a2 in
               let t=(a1-1)*(a2-1) in
               let e=(case (10^20 < n) of
                        True-> egen t 10
                        False-> egen t 5) in
               let d= dgen (t `quot` e ) e t in
               do{
               pub n e;
               priv n d }

egen::Integer->Integer->Integer
egen limit base= case (mygcd limit base) of
                1->base
                otherwise-> case (base `mod` 2) of
                               0->egen limit (base+1)
                               1->egen limit (base+2)

dgen::Integer->Integer->Integer->Integer
dgen d e t = case ((e*d) `mod` t) of
                1-> d
                otherwise->case (d `mod` 2) of
                            1->dgen (d+2) e t
                            0->dgen (d+1) e t
pub n e = do {
                  putStr "Public Key: ";
                   print (n,e);
                   hFlush stdout
                }

priv n d = do {
                  putStr "Private Key: ";
                   print (n,d);
                   hFlush stdout
                }


