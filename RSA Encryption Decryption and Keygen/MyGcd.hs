module MyGcd ( mygcd ) where
import System.Environment
import Data.Int
main = do
    args <- getArgs
    (print $ ((argtoint(args))))
argtoint :: [String]->Integer
argtoint a = do
        gcd (read (head a) :: Integer) (read (head (tail a)) :: Integer)


mygcd :: Integer ->Integer ->Integer
mygcd a b | (a==0 || b==0) = a+b
          | a==b = a
          | a>b = (head (myfilter (factor a) (factorlist b b)))

myfilter :: (a -> Bool) -> [a] -> [a]
myfilter f [] = []
myfilter f (x:xs) | f x == True = x : myfilter f xs
                  | otherwise = myfilter f xs

factor a b | (a `mod` b) ==0 = True
           | otherwise = False

factorlist max index | index < 1 = []
                     |  (max `mod` index)==0 = index : factorlist max (index-1)
                     | otherwise = factorlist max (index-1)

