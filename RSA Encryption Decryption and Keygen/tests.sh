#!/bin/bash
echo "Running tests"
case $1 in 
    clean)

rm -f *.hi *.o.
rm -f primecheck MyGcd keygen primegen encrypt decrypt
ghc -O2 primecheck.hs
ghc -O2 primegen.hs
ghc -O2 MyGcd.hs
ghc -O2 keygen.hs
ghc encrypt.hs
ghc decrypt.hs
;;
    *)
        ;;
esac
cat log.txt >>longlog.txt
rm -f log.txt
echo "Generating 32 bit prime"
./primegen 32 >> log.txt
echo "Generating 64 bit prime"
./primegen 64  >> log.txt
echo "Generating 128 bit prime"
./primegen 128 >> log.txt
echo "Generating 512 bit prime"
./primegen 512 >> log.txt
echo "Generating 1024 bit prime"
./primegen 1024 >> log.txt
while read line; do
    echo "Testing primes...";
    ./primecheck "$line" ;
done < "log.txt"
echo "Proving that primes are unique.."
rm -f proof.txt
cat longlog.txt |
sort            |
uniq -c         > proof.txt
echo "Generating public and private key pairs"
while read line; do
    echo "Testing pair $line";
    ./keygen $line;
done < "testsforkeygen.txt"
echo "Running Test Encryptions"
./encrypt 1040399	7	99
./encrypt 1199021	5	70
./encrypt 216067	5	89	
./encrypt 1127843	7	98	 
./encrypt 1461617	7	113	 
./encrypt 105481	5	105	 
./encrypt 193997	5	85
./encrypt 8023586571293500811  7 88
./encrypt 9453843749223046399 7 102
./encrypt 5695305688669321969 5 205
echo "Running Test Decryptions"
./decrypt 1040399 890023 16560
./decrypt 1199021	478733	901767
./decrypt 216067	172109	169487
./decrypt 1127843	964903	539710	 
./decrypt 1461617	1250743	93069	 
./decrypt 105481	41933	78579	 
./decrypt 193997	154493	1583	
