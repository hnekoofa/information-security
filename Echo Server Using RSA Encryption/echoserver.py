#!/usr/bin/env python

import socket
import subprocess
host = ''
port = 8080
backlog = 50
size = 1024
serverpubkey=("10142701089716483","5")
serverprivkey=("10142701089716483","6085620532976717")
clientpubkey=("10142789312725007","5")
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((host,port))
s.listen(backlog)
while 1:
    client, address = s.accept()
    data = client.recv(size)
    if data:
        decrypt=subprocess.check_output(["./decrypt",serverprivkey[0],serverprivkey[1],data])
        encrypt=subprocess.check_output(["./encrypt",clientpubkey[0],clientpubkey[1],decrypt])
        print data,decrypt,encrypt
        client.send(encrypt)
    client.close()
