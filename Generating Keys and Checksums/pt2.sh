#!/bin/bash
#Encrypt Answer File
#openssl aes-256-ecb -in Ryan-Attard-HW4-part-ii.txt -out Ryan-Attard-HW4-part-ii.aes
#Sign and Signature for Encrypted File
#openssl dgst -sha256 -sign Ryan-Attard-privateKey.pem  -out Ryan-Attard-HW4-part-ii.sha256 Ryan-Attard-HW4-part-ii.aes
#Decrypt File
#openssl enc -d -aes-256-ecb  -in Ryan-Attard-HW4-part-ii.aes 
#Generate pubkey from certificate
openssl x509 -pubkey -noout -in Ryan-Attard-cert.pem > pubkey.pem
#Verify file against signature file
openssl dgst -sha256 -verify pubkey.pem -signature Ryan-Attard-HW4-part-ii.sha256 Ryan-Attard-HW4-part-ii.aes 
