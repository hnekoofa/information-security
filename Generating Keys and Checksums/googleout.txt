dhcp21:Downloads byron$ openssl s_client -connect google.com:443
CONNECTED(00000003)
depth=1 C = US, O = Google Inc, CN = Google Internet Authority
verify error:num=20:unable to get local issuer certificate
verify return:0
---
Certificate chain
 0 s:/C=US/ST=California/L=Mountain View/O=Google Inc/CN=*.google.com
   i:/C=US/O=Google Inc/CN=Google Internet Authority
 1 s:/C=US/O=Google Inc/CN=Google Internet Authority
   i:/C=US/O=Equifax/OU=Equifax Secure Certificate Authority
---
Server certificate
-----BEGIN CERTIFICATE-----
MIIF5DCCBU2gAwIBAgIKFm8TkgAAAABqGTANBgkqhkiG9w0BAQUFADBGMQswCQYD
VQQGEwJVUzETMBEGA1UEChMKR29vZ2xlIEluYzEiMCAGA1UEAxMZR29vZ2xlIElu
dGVybmV0IEF1dGhvcml0eTAeFw0xMjA5MjcwMTIzMDVaFw0xMzA2MDcxOTQzMjda
MGYxCzAJBgNVBAYTAlVTMRMwEQYDVQQIEwpDYWxpZm9ybmlhMRYwFAYDVQQHEw1N
b3VudGFpbiBWaWV3MRMwEQYDVQQKEwpHb29nbGUgSW5jMRUwEwYDVQQDFAwqLmdv
b2dsZS5jb20wgZ8wDQYJKoZIhvcNAQEBBQADgY0AMIGJAoGBAOGFevPAlgthZVz/
9f+ZRbkczukcIl0tIwaNGLC6KBB1wd1xtnIoy1BUx7f8m3LZ22IgQKrJRpXavMFi
FMtPSttpUt49r1Y0MXUCnrVkyiP8AG/uvJshrtzcbT4Te8mD7uFE+tDCFYmuPyOe
mypqJuTaxitV7HA0jiqVdVcjnIMxAgMBAAGjggO3MIIDszAdBgNVHSUEFjAUBggr
BgEFBQcDAQYIKwYBBQUHAwIwHQYDVR0OBBYEFCYnztGTo02Ebr8dghNJnVkVUHpb
MB8GA1UdIwQYMBaAFL/AMOv1QxE+Z7qekfv8atrjaxIkMFsGA1UdHwRUMFIwUKBO
oEyGSmh0dHA6Ly93d3cuZ3N0YXRpYy5jb20vR29vZ2xlSW50ZXJuZXRBdXRob3Jp
dHkvR29vZ2xlSW50ZXJuZXRBdXRob3JpdHkuY3JsMGYGCCsGAQUFBwEBBFowWDBW
BggrBgEFBQcwAoZKaHR0cDovL3d3dy5nc3RhdGljLmNvbS9Hb29nbGVJbnRlcm5l
dEF1dGhvcml0eS9Hb29nbGVJbnRlcm5ldEF1dGhvcml0eS5jcnQwDAYDVR0TAQH/
BAIwADCCAn0GA1UdEQSCAnQwggJwggwqLmdvb2dsZS5jb22CCmdvb2dsZS5jb22C
DSoueW91dHViZS5jb22CC3lvdXR1YmUuY29tghYqLnlvdXR1YmUtbm9jb29raWUu
Y29tggh5b3V0dS5iZYILKi55dGltZy5jb22CDyouZ29vZ2xlLmNvbS5icoIOKi5n
b29nbGUuY28uaW6CCyouZ29vZ2xlLmVzgg4qLmdvb2dsZS5jby51a4ILKi5nb29n
bGUuY2GCCyouZ29vZ2xlLmZyggsqLmdvb2dsZS5wdIILKi5nb29nbGUuaXSCCyou
Z29vZ2xlLmRlggsqLmdvb2dsZS5jbIILKi5nb29nbGUucGyCCyouZ29vZ2xlLm5s
gg8qLmdvb2dsZS5jb20uYXWCDiouZ29vZ2xlLmNvLmpwggsqLmdvb2dsZS5odYIP
Ki5nb29nbGUuY29tLm14gg8qLmdvb2dsZS5jb20uYXKCDyouZ29vZ2xlLmNvbS5j
b4IPKi5nb29nbGUuY29tLnZugg8qLmdvb2dsZS5jb20udHKCDSouYW5kcm9pZC5j
b22CC2FuZHJvaWQuY29tghQqLmdvb2dsZWNvbW1lcmNlLmNvbYISZ29vZ2xlY29t
bWVyY2UuY29tghAqLnVybC5nb29nbGUuY29tggwqLnVyY2hpbi5jb22CCnVyY2hp
bi5jb22CFiouZ29vZ2xlLWFuYWx5dGljcy5jb22CFGdvb2dsZS1hbmFseXRpY3Mu
Y29tghIqLmNsb3VkLmdvb2dsZS5jb22CBmdvby5nbIIEZy5jb4INKi5nc3RhdGlj
LmNvbYIPKi5nb29nbGVhcGlzLmNuMA0GCSqGSIb3DQEBBQUAA4GBALn4OvyJbe9X
32ddF0iHUNXfTVTc6f8kvhs+ySFJJt7ABr2EX87rwNZtiF+0Tcze8b1ChvvcZjwU
+HMOUpNdLZcL8U90eh4Nqb/D3pZyZEsL6iI6QG53L+ETDGz05ulU00PMOAxVPEec
c5k6y78OSYT5ejP1OqXqJURtecm+op/N
-----END CERTIFICATE-----
subject=/C=US/ST=California/L=Mountain View/O=Google Inc/CN=*.google.com
issuer=/C=US/O=Google Inc/CN=Google Internet Authority
---
No client certificate CA names sent
---
SSL handshake has read 2722 bytes and written 444 bytes
---
New, TLSv1/SSLv3, Cipher is ECDHE-RSA-RC4-SHA
Server public key is 1024 bit
Secure Renegotiation IS supported
Compression: NONE
Expansion: NONE
SSL-Session:
    Protocol  : TLSv1.2
    Cipher    : ECDHE-RSA-RC4-SHA
    Session-ID: A5CB1E8167BF8F2C20798C908FBE7762C0B7690833DC98BA81F8686811115A17
    Session-ID-ctx: 
    Master-Key: 85F75059E8B84C90913C5C7E544BE5D6A3E8592822A6A309C93D64C6734D7CF922E83CB7D7C5AC1071F4C4DF26523B20
    Key-Arg   : None
    PSK identity: None
    PSK identity hint: None
    SRP username: None
    TLS session ticket lifetime hint: 100800 (seconds)
    TLS session ticket:
    0000 - ba db b4 a5 65 9f a6 e3-e7 3a fd 3c 5e 9d 23 3f   ....e....:.<^.#?
    0010 - e1 4f da 64 ee 04 41 a1-37 a0 93 7c 2f 7d fb 08   .O.d..A.7..|/}..
    0020 - 1c 64 d8 c3 5d a4 be e9-d3 b3 04 43 b9 ac a1 4b   .d..]......C...K
    0030 - b6 fc 0a e0 5d de 42 7c-a2 6c 9d 6d 6e fb ad 70   ....].B|.l.mn..p
    0040 - cd 0f f4 c7 21 a7 14 32-68 19 e3 a0 98 2f c3 e5   ....!..2h..../..
    0050 - 1d f1 cc ac c4 ab e0 b6-55 47 b3 09 d4 e3 4d 58   ........UG....MX
    0060 - d7 ff f4 14 c6 c1 9c 6e-70 b6 97 52 7b 10 3e f4   .......np..R{.>.
    0070 - 05 b4 2c 60 7b d9 06 39-8e 83 1d 8b 68 80 38 4e   ..,`{..9....h.8N
    0080 - 82 98 f0 60 bf a0 55 ca-db e7 0a 8b fa 0f b6 0a   ...`..U.........
    0090 - ad 49 19 9a                                       .I..

    Start Time: 1350340869
    Timeout   : 300 (sec)
    Verify return code: 20 (unable to get local issuer certificate)
---
get \
HTTP/1.0 400 Bad Request
Content-Type: text/html; charset=UTF-8
Content-Length: 925
Date: Mon, 15 Oct 2012 22:41:11 GMT
Server: GFE/2.0

<!DOCTYPE html>
<html lang=en>
  <meta charset=utf-8>
  <meta name=viewport content="initial-scale=1, minimum-scale=1, width=device-width">
  <title>Error 400 (Bad Request)!!1</title>
  <style>
    *{margin:0;padding:0}html,code{font:15px/22px arial,sans-serif}html{background:#fff;color:#222;padding:15px}body{margin:7% auto 0;max-width:390px;min-height:180px;padding:30px 0 15px}* > body{background:url(//www.google.com/images/errors/robot.png) 100% 5px no-repeat;padding-right:205px}p{margin:11px 0 22px;overflow:hidden}ins{color:#777;text-decoration:none}a img{border:0}@media screen and (max-width:772px){body{background:none;margin-top:0;max-width:none;padding-right:0}}
  </style>
  <a href=//www.google.com/><img src=//www.google.com/images/errors/logo_sm.gif alt=Google></a>
  <p><b>400.</b> <ins>That’s an error.</ins>
  <p>Your client has issued a malformed or illegal request.  <ins>That’s all we know.</ins>
read:errno=0
dhcp21:Downloads byron$ openssl s_client -connect google.com:443
