import java.math.*;

public class gcd {

    /**
     * @param args
     */
    public static void main(String[] args) {
	if (args.length == 2) {
	    BigInteger t1 = new BigInteger(args[0]);
	    BigInteger t2 = new BigInteger(args[1]);
	    System.out.println(gcdfun(t1, t2));

	} else {
	    BigInteger t1 = new BigInteger("586898689868986900219865");
	    BigInteger t2 = new BigInteger("4323874085395");
	    System.out.println(gcdfun(t1, t2));

	}
	return;
    }

    public static BigInteger gcdfun(BigInteger a, BigInteger b) {
	BigInteger gcd = new BigInteger("1");
	// Just truncate the values off of the top for an approximate limit
	BigInteger limit = a.min(b);
	for (BigInteger i = gcd; (i.compareTo(limit) < 0); i = i
		.add(BigInteger.ONE)) {
	    BigInteger remvala = a.divideAndRemainder(i)[1];
	    BigInteger remvalb = b.divideAndRemainder(i)[1];
	    if (remvalb.equals(remvala) && remvala.equals(BigInteger.ZERO)) {
		gcd = i;
	    }
	}
	return gcd;
    }
}
